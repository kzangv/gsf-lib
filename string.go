package glib

import (
	"bytes"
	"golang.org/x/exp/constraints"
	"strconv"
)

// SubStr 获取子字符串
func SubStr(str string, start int, end int) string {
	rs := []rune(str)
	length := len(rs)
	if start < 0 || start > length {
		return ""
	}
	if end < 0 || end > length {
		return ""
	}
	return string(rs[start:end])
}

func StringMerge(str ...string) string {
	var buff bytes.Buffer
	for k := range str {
		buff.WriteString(str[k])
	}
	return buff.String()
}

// JoinInt join 有符号整形
func JoinInt[T constraints.Signed](s []T, sep string) string {
	var buffer bytes.Buffer
	sLen := len(s)
	sepLen := len(sep)
	for k, v := range s {
		buffer.WriteString(strconv.FormatInt(int64(v), 10))
		if sepLen != 0 && k+1 != sLen {
			buffer.WriteString(sep)
		}
	}
	return buffer.String()
}

// JoinUint join 无符号整形
func JoinUint[T constraints.Unsigned](s []T, sep string) string {
	var buffer bytes.Buffer
	sLen := len(s)
	sepLen := len(sep)
	for k, v := range s {
		buffer.WriteString(strconv.FormatUint(uint64(v), 10))
		if sepLen != 0 && k+1 != sLen {
			buffer.WriteString(sep)
		}
	}
	return buffer.String()
}

// JoinFloat join 浮点型
func JoinFloat[T constraints.Float](s []T, sep string, prec int) string {
	var buffer bytes.Buffer
	sLen := len(s)
	sepLen := len(sep)
	for k, v := range s {
		buffer.WriteString(strconv.FormatFloat(float64(v), 'f', prec, 64))
		if sepLen != 0 && k+1 != sLen {
			buffer.WriteString(sep)
		}
	}
	return buffer.String()
}
