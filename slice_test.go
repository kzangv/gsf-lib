package glib

import "testing"

type node struct {
	id int
}

func TestSlice(t *testing.T) {

	t.Log("slice 数据集操作")

	r := []int{1, 2, 3}

	t.Log("slice 数据转换 -----")
	t.Logf("Slice2Map %v", Slice2Map(r, func(v *int) int { return *v }))
	t.Logf("Slice2MapFilter %v", Slice2MapFilter(r, func(v *int) (int, bool) {
		if *v == 2 {
			return 0, false
		}
		return *v, true
	}))
	t.Logf("Slice2MapEx %v", Slice2MapEx(r, func(v *int) (int, string) { return *v, "k" + ToString(*v) }))
	t.Log("Slice2MapFilterEx 使用参考 Slice2MapFilter")
	t.Logf("Slice2Set %v", Slice2Set(r, func(v *int) int { return *v }))
	t.Log("Slice2SetFilter 使用参考 Slice2MapFilter")
	t.Logf("Slice2Slice %v", Slice2Slice(r, func(v *int) int { return *v + 10 }))
	t.Log("Slice2SliceFilter 使用参考 Slice2MapFilter")

	t.Log("slice 基础操作 -----")
	t.Logf("SliceUnique %v", SliceUnique([]int{1, 1, 3, 2, 2}))
	ret := SliceForEachIdx(len(r), func(i int) bool {
		t.Logf("--- v:%d", r[i])
		if r[i] == 2 { // 在值等于 2 的元素中止遍历
			return false
		}
		return true
	})
	t.Logf("SliceForeach %v", ret)
	t.Logf("SliceSortAsc %v", SliceSortAsc(r))
	t.Logf("SliceSortDesc %v", SliceSortDesc(r))

	t.Log("slice 数据集操作 -----")
	t.Logf("SliceIntersect 交集 %v", SliceIntersect([]int{1, 1, 2, 3, 5, 6}, []int{1, 1, 5, 10, 12}, []int{3, 1, 5, 8, 1}))
	t.Logf("SliceDifference 差集 %v", SliceDifference([]int{1, 1, 2, 3, 5, 6}, []int{1, 5, 10, 12}, []int{3, 1, 5, 8, 1}))
	t.Logf("SliceUnion 并集 %v", SliceUnion([]int{1, 2, 3, 5, 6}, []int{1, 5, 10, 12}, []int{3, 5, 8, 1}))
	t.Logf("SliceUnion Sub %v", SliceSub([]int{1, 2, 3, 5, 6}, []int{1, 5, 10, 12}, []int{3}))
	t.Logf("SliceUnion Add %v", SliceAdd([]int{1, 2, 3, 5, 6}, []int{1, 5, 10, 12}, []int{3}))

	t.Logf("SliceIntersect 交集 %v", Slice2Slice(SliceIntersectEx(func(v *node) int { return v.id },
		[]node{{1}, {1}, {2}, {3}, {5}, {6}},
		[]node{{1}, {5}, {10}, {12}},
		[]node{{3}, {1}, {5}, {8}, {1}}), func(v **node) node { return **v }))
	t.Logf("SliceDifference 差集 %v", Slice2Slice(SliceDifferenceEx(func(v *node) int { return v.id },
		[]node{{1}, {1}, {2}, {3}, {5}, {6}},
		[]node{{1}, {5}, {10}, {12}},
		[]node{{3}, {1}, {5}, {8}, {1}}), func(v **node) node { return **v }))
	t.Logf("SliceUnion 并集 %v", Slice2Slice(SliceUnionEx(func(v *node) int { return v.id },
		[]node{{1}, {1}, {2}, {3}, {5}, {6}},
		[]node{{1}, {5}, {10}, {12}},
		[]node{{3}, {1}, {5}, {8}, {1}}), func(v **node) node { return **v }))
}
