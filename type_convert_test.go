package glib

import (
	"testing"
)

func TestTypeConvert(t *testing.T) {
	// IntegerToString
	if v := IntegerToString[int](123); v == "123" {
		t.Logf("IntegerToString(int) %v", v)
	} else {
		t.Errorf("IntegerToString convert failed")
	}

	// FloatToString
	if v := FloatToString[float32](123.2222, 2); v == "123.22" {
		t.Logf("FloatToString(int) %v", v)
	} else {
		t.Errorf("FloatToString convert failed")
	}

	// StringToInteger
	if v := StringToInteger("123"); v == 123 {
		t.Logf("StringToInteger(int) %v", v)
	} else {
		t.Errorf("StringToInteger convert failed")
	}

	// StringToFloat
	if v := StringToFloat("123.22"); v == 123.22 {
		t.Logf("StringToFloat(int) %v", v)
	} else {
		t.Errorf("StringToFloat convert failed")
	}

	// ToFloat
	var f float64
	if f = ToFloat("123.11111"); f != 123.11111 {
		t.Errorf("float convert failed")
	}
	t.Logf("ToFloat(string) %v", f)
	if f = ToFloat(123); f != 123 {
		t.Errorf("float convert failed")
	}
	t.Logf("ToFloat(int) %v", f)

	// toInt
	var i int
	if i = ToInt(float32(32.22)); i != 32 {
		t.Errorf("int convert failed")
	}
	t.Logf("ToInt(float) %v", i)
	if i = ToInt("32"); i != 32 {
		t.Errorf("int convert failed")
	}
	t.Logf("ToInt(string) %v", i)

	// toString
	var s string
	if s = ToString(123.11111, 2); s != "123.11" {
		t.Errorf("string convert failed")
	}
	t.Logf("ToString(float) %v", s)
	if s = ToString(123); s != "123" {
		t.Errorf("string convert failed")
	}
	t.Logf("ToString(int) %v", s)
}
