package glib

import (
	"math/rand"
)

const (
	RandStrNumberMask    = 0x01
	RandStrUpperCaseMask = 0x02
	RandStrLowerCaseMask = 0x04
	_RandStrNumber       = "0123456789"
	_RandStrUpperCase    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	_RandStrLowerCase    = "abcdefghijklmnopqrstuvwxyz"
)

//RandInt 生成范围内的 int
func RandInt(min, max int) int {
	if min > max {
		min, max = max, min
	}
	if min == max {
		return min
	}
	return rand.Intn(max-min) + min
}

//RandString 生成随机字符串
func RandString(length int, chars string, mod uint8) string {
	vs, vl := make([]string, 0, 4), 0
	if len(chars) > 0 {
		vs, vl = append(vs, chars), vl+len(chars)
	}
	if mod&RandStrNumberMask > 0 {
		vs, vl = append(vs, _RandStrNumber), vl+len(_RandStrNumber)
	}
	if mod&RandStrUpperCaseMask > 0 {
		vs, vl = append(vs, _RandStrUpperCase), vl+len(_RandStrUpperCase)
	}
	if mod&RandStrLowerCaseMask > 0 {
		vs, vl = append(vs, _RandStrLowerCase), vl+len(_RandStrLowerCase)
	}
	result := make([]byte, length)
	for i := 0; i < length; i++ {
		rl := rand.Intn(vl)
		for j := range vs {
			jl := len(vs[j])
			if jl > rl {
				result[i] = vs[j][rl]
				break
			} else {
				rl = rl - jl
			}
		}
	}
	return string(result)
}

func RandStringEx(length int) string {
	return RandString(length, "", RandStrNumberMask|RandStrLowerCaseMask|RandStrUpperCaseMask)
}
