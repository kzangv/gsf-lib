package glib

import "testing"

func TestMap(t *testing.T) {
	v := map[string]int{
		"k1": 1,
		"k2": 2,
		"k3": 3,
		"k4": 4,
	}

	t.Logf("Map2Slice %v", Map2Slice(v, func(k string, v int) int { return v + 10 }))
	t.Logf("MapKeys %v", MapKeys(v))
	t.Logf("MapValues %v", MapValues(v))
}
