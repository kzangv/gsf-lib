package glib

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"io/ioutil"
)

func ParseGzip(data []byte) ([]byte, error) {
	b := new(bytes.Buffer)
	err := binary.Write(b, binary.LittleEndian, data)
	if err != nil {
		return nil, err
	}
	read, err := gzip.NewReader(b)
	if err != nil {
		return nil, err
	}
	defer read.Close()
	rawData, err := ioutil.ReadAll(read)
	if err != nil {
		return nil, err
	}
	return rawData, nil
}
