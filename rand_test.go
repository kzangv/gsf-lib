package glib

import (
	"math/rand"
	"testing"
	"time"
)

func TestRand(t *testing.T) {
	rand.Seed(time.Now().Unix())
	for i, l := 0, 5; i < l; i++ {
		t.Logf("rand int: %d", RandInt(50, 100))
	}
	for i, l := 0, 5; i <= l; i++ {
		t.Logf("rand string（%d）: %s", i+5, RandStringEx(i+5))
	}
}
