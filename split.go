package glib

import (
	"golang.org/x/exp/constraints"
	"strconv"
	"strings"
)

// SplitInt split 切割 int
func SplitInt[T constraints.Signed](s, sep string) ([]T, error) {
	ss := strings.Split(s, sep)
	r := make([]T, 0, len(ss))
	for _, v := range ss {
		iv, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			return nil, err
		}
		r = append(r, T(iv))
	}
	return r, nil
}

// SplitUint split 切割 uint
func SplitUint[T constraints.Unsigned](s, sep string) ([]T, error) {
	ss := strings.Split(s, sep)
	r := make([]T, 0, len(ss))
	for _, v := range ss {
		iv, err := strconv.ParseUint(v, 10, 64)
		if err != nil {
			return nil, err
		}
		r = append(r, T(iv))
	}
	return r, nil
}

// SplitFloat split 切割 float
func SplitFloat[T constraints.Float](s, sep string) ([]T, error) {
	ss := strings.Split(s, sep)
	r := make([]T, 0, len(ss))
	for _, v := range ss {
		iv, err := strconv.ParseFloat(v, 32)
		if err != nil {
			return nil, err
		}
		r = append(r, T(iv))
	}
	return r, nil
}

/* SplitSlice *******************************/

// SplitSliceData split 切割 slice 数据结构
type SplitSliceData interface {
	Len() int
	Compare(i, j int) bool
}

// SplitSlice split 切割 slice
func SplitSlice(v SplitSliceData, sHandle func(b, e int) error) error {
	for b, i, l := 0, 1, v.Len(); i <= l; b, i = i, i+1 {
		for i < l && v.Compare(b, i) {
			i++
		}
		if err := sHandle(b, i); err != nil {
			return err
		}
	}
	return nil
}

// SplitSliceEx split 切割 slice
func SplitSliceEx(v SplitSliceData, sHandle func(b, e int) error, eHandle func(i int) error) error {
	for b, i, l := 0, 1, v.Len(); i <= l; b, i = i, i+1 {
		if err := eHandle(b); err != nil {
			return err
		}
		for ; i < l && v.Compare(b, i); i++ {
			if err := eHandle(i); err != nil {
				return err
			}
		}
		if err := sHandle(b, i); err != nil {
			return err
		}
	}
	return nil
}

// SplitSliceTmp split 切割 slice
func SplitSliceTmp[T any](v []T, cHandle func(a, b *T) bool, sHandle func(b, e int) error) error {
	for b, i, l := 0, 1, len(v); i <= l; b, i = i, i+1 {
		for i < l && cHandle(&v[b], &v[i]) {
			i++
		}
		if err := sHandle(b, i); err != nil {
			return err
		}
	}
	return nil
}

// SplitSliceTmpEx split 切割 slice
func SplitSliceTmpEx[T any](v []T, cHandle func(a, b *T) bool, sHandle func(b, e int) error, eHandle func(i int) error) error {
	for b, i, l := 0, 1, len(v); i <= l; b, i = i, i+1 {
		if err := eHandle(b); err != nil {
			return err
		}
		for ; i < l && cHandle(&v[b], &v[i]); i++ {
			if err := eHandle(i); err != nil {
				return err
			}
		}
		if err := sHandle(b, i); err != nil {
			return err
		}
	}
	return nil
}
