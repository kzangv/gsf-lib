package glib

type EmptyStruct struct{}

type JsonRawByte []byte

func (v JsonRawByte) MarshalJSON() ([]byte, error) {
	if v == nil {
		return []byte("null"), nil
	}
	return v, nil
}

func (v *JsonRawByte) UnmarshalJSON(bytes []byte) error {
	*v = append((*v)[0:0], bytes...)
	return nil
}

func (v *JsonRawByte) String() string {
	return string(*v)
}

func (v *JsonRawByte) Set(val string) {
	*v = []byte(val)
}

type JsonRawUnmarshalByte []byte

func (v *JsonRawUnmarshalByte) UnmarshalJSON(bytes []byte) error {
	*v = append((*v)[0:0], bytes...)
	return nil
}

func (v *JsonRawUnmarshalByte) String() string {
	return string(*v)
}

type JsonRawMarshalByte []byte

func (v JsonRawMarshalByte) MarshalJSON() ([]byte, error) {
	if v == nil {
		return []byte("null"), nil
	}
	return v, nil
}

func (v *JsonRawMarshalByte) Set(val string) {
	*v = []byte(val)
}

type JsonRawString string

func (v *JsonRawString) UnmarshalJSON(bytes []byte) error {
	*v = JsonRawString(bytes)
	return nil
}

func (v JsonRawString) MarshalJSON() ([]byte, error) {
	if v == "" {
		return []byte("null"), nil
	}
	return []byte(v), nil
}

type JsonRawUnmarshalString string

func (s *JsonRawUnmarshalString) UnmarshalJSON(bytes []byte) error {
	*s = JsonRawUnmarshalString(bytes)
	return nil
}

type JsonRawMarshalString string

func (v JsonRawMarshalString) MarshalJSON() ([]byte, error) {
	if v == "" {
		return []byte("null"), nil
	}
	return []byte(v), nil
}
