package glib

import (
	"errors"
	"os"
)

func FileExist(path string) error {
	_, e := os.Stat(path)
	if os.IsNotExist(e) {
		return errors.New("path is not exit:" + path)
	}
	if os.IsPermission(e) {
		return errors.New("path is permission deny:" + path)
	}
	return nil
}

func FilesExist(path ...string) error {
	for _, value := range path {
		err := FileExist(value)
		if err != nil {
			return err
		}
	}
	return nil
}
