package glib

import (
	"testing"
	"time"
)

func TestTime(t *testing.T) {
	//TimeLatelyWeekDay
	for _, v := range []string{"2023-04-17 00:00:00", "2023-04-19 00:00:00", "2023-04-23 00:00:00"} {
		tv, _ := TimeParseInLocation(v, TimeFormatDateTime)
		t.Logf("-----------------------\nNow: %s \n\n", tv.Format(TimeFormatDateTime))
		// TimeLatelyWeekDay: Early
		t.Logf("TimeLatelyWeekDay: Early -------\n")
		t.Logf("Early Tuesday: %s \n", TimeLatelyNWeekDay(tv, time.Tuesday, 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early Sunday: %s \n", TimeLatelyNWeekDay(tv, time.Sunday, 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early CurWeekDay(true): %s \n", TimeLatelyNWeekDay(tv, tv.Weekday(), 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early CurWeekDay(false): %s \n\n", TimeLatelyNWeekDay(tv, tv.Weekday(), 2, TimeLatelyBefore, false).Format(TimeFormatDateTime))

		// TimeLatelyWeekDay: Last
		t.Logf("TimeLatelyWeekDay: Last -------\n")
		t.Logf("Last Tuesday: %s \n", TimeLatelyNWeekDay(tv, time.Tuesday, 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last Sunday: %s \n", TimeLatelyNWeekDay(tv, time.Sunday, 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last CurWeekDay(true): %s \n", TimeLatelyNWeekDay(tv, tv.Weekday(), 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last CurWeekDay(false): %s \n\n", TimeLatelyNWeekDay(tv, tv.Weekday(), 2, TimeLatelyAfter, false).Format(TimeFormatDateTime))

		// TimeLatelyWeekDay: Last
		t.Logf("TimeLatelyWeekDay: In -------\n")
		t.Logf("In Monday: %s \n", TimeThisWeekDay(tv, time.Monday).Format(TimeFormatDateTime))
		t.Logf("In Tuesday: %s \n", TimeThisWeekDay(tv, time.Tuesday).Format(TimeFormatDateTime))
		t.Logf("In Wednesday: %s \n", TimeThisWeekDay(tv, time.Wednesday).Format(TimeFormatDateTime))
		t.Logf("In Saturday: %s \n", TimeThisWeekDay(tv, time.Saturday).Format(TimeFormatDateTime))
		t.Logf("In Sunday: %s \n\n", TimeThisWeekDay(tv, time.Sunday).Format(TimeFormatDateTime))
	}

	//TimeLatelyMonthDay
	for _, v := range []string{"2023-01-30 10:20:30", "2023-02-28 00:00:00", "2023-03-31 00:00:00", "2023-05-15 00:00:00", "2023-12-31 10:20:30"} {
		tv, _ := TimeParseInLocation(v, TimeFormatDateTime)
		t.Logf("-----------------------\nNow: %s \n\n", tv.Format(TimeFormatDateTime))
		// TimeLatelyWeekDay: Early
		t.Logf("TimeLatelyMonthDay: Early -------\n")
		t.Logf("Early 1d: %s \n", TimeLatelyNMonthDay(tv, 1, 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early 30d: %s \n", TimeLatelyNMonthDay(tv, 30, 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early lastDay: %s \n", TimeLatelyNMonthDay(tv, -1, 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early CurMonthDay(true): %s \n", TimeLatelyNMonthDay(tv, tv.Day(), 2, TimeLatelyBefore, true).Format(TimeFormatDateTime))
		t.Logf("Early CurMonthDay(false): %s \n\n", TimeLatelyNMonthDay(tv, tv.Day(), 2, TimeLatelyBefore, false).Format(TimeFormatDateTime))

		// TimeLatelyWeekDay: Last
		t.Logf("TimeLatelyMonthDay: Last -------\n")
		t.Logf("Last 1d: %s \n", TimeLatelyNMonthDay(tv, 1, 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last 30d: %s \n", TimeLatelyNMonthDay(tv, 30, 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last lastDay: %s \n", TimeLatelyNMonthDay(tv, -1, 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last CurMonthDay(true): %s \n", TimeLatelyNMonthDay(tv, tv.Day(), 2, TimeLatelyAfter, true).Format(TimeFormatDateTime))
		t.Logf("Last CurMonthDay(false): %s \n\n", TimeLatelyNMonthDay(tv, tv.Day(), 2, TimeLatelyAfter, false).Format(TimeFormatDateTime))

		// TimeLatelyWeekDay: Last
		t.Logf("TimeLatelyNMonthDay: In -------\n")
		t.Logf("In 1d: %s \n", TimeThisMonthDay(tv, 1).Format(TimeFormatDateTime))
		t.Logf("In 30d: %s \n", TimeThisMonthDay(tv, 30).Format(TimeFormatDateTime))
		t.Logf("In lastDay: %s \n", TimeThisMonthDay(tv, -1).Format(TimeFormatDateTime))
	}

	ts := "2023-04-17 12:34:56.330044001"
	tsv, _ := TimeParseInLocation("2023-04-17 12:34:56.330044001", TimeFormatDateTime+".000000000")
	t.Logf("-----------------------\nNow: %s \n\n", ts)
	t.Logf("Second: %s \n", TimeClearZero(tsv, TimeClearSecond).Format(TimeFormatDateTime+".000000000"))
	t.Logf("Minute: %s \n", TimeClearZero(tsv, TimeClearMinute).Format(TimeFormatDateTime+".000000000"))
	t.Logf("Hour: %s \n", TimeClearZero(tsv, TimeClearHour).Format(TimeFormatDateTime+".000000000"))
	t.Logf("Time: %s \n", TimeClearZero(tsv, TimeClearTime).Format(TimeFormatDateTime+".000000000"))
}
