package glib

import "testing"

type TestIntSlice []int

func (m TestIntSlice) Len() int {
	return len(m)
}
func (m TestIntSlice) Compare(i, j int) bool {
	return m[i] == m[j]
}

func TestSplit(t *testing.T) {
	iv, _ := SplitInt[int]("-1#-2#-3", "#")
	t.Logf("SplitInt: %v", iv)

	uv, _ := SplitUint[uint]("1#2#3", "#")
	t.Logf("SplitUint: %v", uv)

	fv, _ := SplitFloat[float32]("1.1#2.3#3.1", "#")
	t.Logf("SplitUint: %v", fv)

	os, ns := TestIntSlice{1, 1, 1, 3, 3, 5, 5, 5}, make([]TestIntSlice, 0, 3)
	_ = SplitSlice(os, func(b, e int) error {
		ns = append(ns, os[b:e])
		return nil
	})
	t.Logf("SplitSlice: raw: %v, new: %v", os, ns)

	os, ns = TestIntSlice{1, 1, 1, 3, 3, 5, 5, 5}, make([]TestIntSlice, 0, 3)
	_ = SplitSliceEx(os, func(b, e int) error {
		ns = append(ns, os[b:e])
		return nil
	}, func(i int) error {
		t.Logf("SplitSliceExEach: %v-%v", i, os[i])
		return nil
	})
	t.Logf("SplitSliceEx: raw: %v, new: %v", os, ns)

	os, ns = TestIntSlice{1, 1, 1, 3, 3, 5, 5, 5}, make([]TestIntSlice, 0, 3)
	_ = SplitSliceTmp(os,
		func(a, b *int) bool {
			return *a == *b
		},
		func(b, e int) error {
			ns = append(ns, os[b:e])
			return nil
		})
	t.Logf("SplitSlice: raw: %v, new: %v", os, ns)

	os, ns = TestIntSlice{1, 1, 1, 3, 3, 5, 5, 5}, make([]TestIntSlice, 0, 3)
	_ = SplitSliceTmpEx(os,
		func(a, b *int) bool {
			return *a == *b
		},
		func(b, e int) error {
			ns = append(ns, os[b:e])
			return nil
		},
		func(i int) error {
			t.Logf("SplitSliceExEach: %v-%v", i, os[i])
			return nil
		})
	t.Logf("SplitSliceEx: raw: %v, new: %v", os, ns)
}
