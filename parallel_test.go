package glib

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"
)

const HeaderAccessToken = "Access-Token"

func NewGetRequest(token, url string) ParallelHttpNewRequest {
	return func(ctx context.Context) (*http.Request, error) {
		req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
		if err == nil {
			req.Header.Set(HeaderAccessToken, token)
		}
		return req, err
	}
}

func NewPostFormUrlRequest(token, uri string, form map[string]string) ParallelHttpNewRequest {
	return func(ctx context.Context) (*http.Request, error) {
		postData := url.Values{}
		for k, v := range form {
			postData.Add(k, v)
		}
		req, err := http.NewRequestWithContext(ctx, "POST", uri, strings.NewReader(postData.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		if err == nil {
			req.Header.Set(HeaderAccessToken, token)
		}
		return req, err
	}
}

func NewPostFormRequest(token, url string, form map[string]string) ParallelHttpNewRequest {
	return func(ctx context.Context) (*http.Request, error) {
		body := new(bytes.Buffer)
		w := multipart.NewWriter(body)
		for k, v := range form {
			_ = w.WriteField(k, v)
		}
		w.Close()

		req, err := http.NewRequestWithContext(ctx, "POST", url, body)
		req.Header.Set("Content-Type", w.FormDataContentType())
		if err == nil {
			req.Header.Set(HeaderAccessToken, token)
		}
		return req, err
	}
}

func RepReader(title string) ParallelHttpResponseFormat {
	return func(rbody io.ReadCloser) error {
		body, _ := ioutil.ReadAll(rbody)
		strBody := string(body)
		if len(strBody) > 30 {
			strBody = strBody[0:20] + "..."
		}
		fmt.Println(title + ": " + strBody)
		return nil
	}
}

func TestParallel(t *testing.T) {
	mgt := ParallelMgt{}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
			DisableCompression: true,
			Proxy:              http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   5 * time.Second,
				KeepAlive: 5 * time.Second,
			}).DialContext,
			MaxIdleConns:          20,
			MaxIdleConnsPerHost:   5,
			IdleConnTimeout:       time.Duration(30) * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
		Timeout: 120 * time.Second,
	}

	mgt.AddFunc(func() error {
		time.Sleep(time.Second * 1)
		t.Logf("%s: fun1-done", time.Now().Format(TimeFormatTime))
		return nil
	})

	mgt.AddHandles(ParallelGetFunc(func() error {
		time.Sleep(time.Second * 1)
		t.Logf("%s: fun4-done", time.Now().Format(TimeFormatTime))
		return fmt.Errorf("fun4-err")
	}), func(err error) {
		t.Logf("%s: fun4-err(%s)", time.Now().Format(TimeFormatTime), err.Error())
	})

	mgt.AddHandles(ParallelGetFunc(func() error {
		time.Sleep(time.Second * 3)
		t.Logf("%s: fun2-done", time.Now().Format(TimeFormatTime))
		return nil
	}), func(err error) {
		t.Logf("%s: fun2-err(%s)", time.Now().Format(TimeFormatTime), err.Error())
	})

	mgt.AddHandles(ParallelGetFunc(func() error {
		time.Sleep(time.Second * 5)
		t.Logf("%s: fun3-done", time.Now().Format(TimeFormatTime))
		return nil
	}), func(err error) {
		t.Logf("%s: fun3-err(%s)", time.Now().Format(TimeFormatTime), err.Error())
	})

	host, token := "http://127.0.0.1:8889/newgoapi", ""

	mgt.AddHttpRequest(client,
		NewGetRequest(token, fmt.Sprintf(host+"/site/getVersion")),
		RepReader("/site/getVersion"))

	for _, v := range []ParallelFailType{ParallelFailTypeOne} {
		t.Logf("%s: ParallelFailType: %d\n", time.Now().Format(TimeFormatTime), v)
		errs := mgt.Run(time.Second*20, v)
		t.Logf("%+v\n\n", errs)
	}
}
