package glib

import (
	"fmt"
	"net"
	"net/http"
	"strings"
)

func Ip2Long(ipStr string) uint32 {
	ip := net.ParseIP(ipStr)
	a := uint32(ip[12])
	b := uint32(ip[13])
	c := uint32(ip[14])
	d := uint32(ip[15])
	return uint32(a<<24 | b<<16 | c<<8 | d)
}

// NetLong2ip 获得 ip 地址
func NetLong2ip(ip uint32) string {
	return fmt.Sprintf("%d.%d.%d.%d", ip>>24, ip<<8>>24, ip<<16>>24, ip<<24>>24)
}

// GetClientIp 获取客户端 ip 地址
func GetClientIp(r *http.Request) string {
	forwardedFor := r.Header.Get("X-Forwarded-For")
	if forwardedFor != "" {
		addr := strings.Split(strings.TrimSpace(forwardedFor), ",")
		fip := net.ParseIP(addr[0])
		if fip != nil && IsPublicIP(fip) {
			return addr[0]
		}
	}

	Remoteip := r.Header.Get("Remoteip")
	if Remoteip != "" {
		rip := net.ParseIP(Remoteip)
		if rip != nil && IsPublicIP(rip) {
			return Remoteip
		}
	}

	wlProxyClientIp := r.Header.Get("WL-Proxy-Client-Host")
	if wlProxyClientIp != "" {
		wip := net.ParseIP(wlProxyClientIp)
		if wip != nil && IsPublicIP(wip) {
			return wlProxyClientIp
		}
	}

	XRealIP := r.Header.Get("X-Real-Ip")
	if XRealIP != "" {
		xip := net.ParseIP(XRealIP)
		if xip != nil && IsPublicIP(xip) {
			return XRealIP
		}
	}

	ClientIP := r.Header.Get("HTTP_CLIENT_IP")
	if ClientIP != "" {
		cip := net.ParseIP(ClientIP)
		if cip != nil && IsPublicIP(cip) {
			return ClientIP
		}
	}

	RemoteAdd := r.Header.Get("Remote_add")
	if RemoteAdd != "" {
		mip := net.ParseIP(RemoteAdd)
		if mip != nil && IsPublicIP(mip) {
			return RemoteAdd
		}
	}

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)

	return ip
}

// IsPublicIP 是否是外网 ip
func IsPublicIP(IP net.IP) bool {
	if IP.IsLoopback() || IP.IsLinkLocalMulticast() || IP.IsLinkLocalUnicast() {
		return false
	}
	if ip4 := IP.To4(); ip4 != nil {
		switch true {
		case ip4[0] == 10:
			return false
		case ip4[0] == 172 && ip4[1] >= 16 && ip4[1] <= 31:
			return false
		case ip4[0] == 192 && ip4[1] == 168:
			return false
		default:
			return true
		}
	}
	return false
}
