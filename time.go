package glib

import "time"

const (
	TimeFormatDateTime       = "2006-01-02 15:04:05"
	TimeFormatDate           = "2006-01-02"
	TimeFormatDateYearMonth  = "2006-01"
	TimeFormatTime           = "15:04:05"
	TimeFormatTimeHourMinute = "15:04"
	TimeFormatANSIC          = time.ANSIC       // "Mon Jan _2 15:04:05 2006"
	TimeFormatUnixDate       = time.UnixDate    // "Mon Jan _2 15:04:05 MST 2006"
	TimeFormatRubyDate       = time.RubyDate    // "Mon Jan 02 15:04:05 -0700 2006"
	TimeFormatRFC822         = time.RFC822      // "02 Jan 06 15:04 MST"
	TimeFormatRFC822Z        = time.RFC822Z     // "02 Jan 06 15:04 -0700"
	TimeFormatRFC850         = time.RFC850      // "Monday, 02-Jan-06 15:04:05 MST"
	TimeFormatRFC1123        = time.RFC1123     // "Mon, 02 Jan 2006 15:04:05 MST"
	TimeFormatRFC1123Z       = time.RFC1123Z    // "Mon, 02 Jan 2006 15:04:05 -0700"
	TimeFormatRFC3339        = time.RFC3339     // "2006-01-02T15:04:05Z07:00"
	TimeFormatRFC3339Nano    = time.RFC3339Nano // "2006-01-02T15:04:05.999999999Z07:00"
	TimeFormatKitchen        = time.Kitchen     // "3:04PM"
)

type TimeLately int

const (
	TimeLatelyBefore TimeLately = 1
	TimeLatelyAfter  TimeLately = 2
	_TimeLatelyIn    TimeLately = 3
)

const (
	TimeClearNSecondMask = 0x01
	TimeClearSecondMask  = 0x02
	TimeClearMinuteMask  = 0x04
	TimeClearHourMask    = 0x08

	TimeClearSecond = TimeClearNSecondMask
	TimeClearMinute = TimeClearSecondMask | TimeClearSecond
	TimeClearHour   = TimeClearMinuteMask | TimeClearMinute
	TimeClearTime   = TimeClearHourMask | TimeClearHour
)

var (
	_TimeYearMonthDays = []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
)

func TimeIsLeapYear(year int) bool {
	return year%4 == 0 && (year%100 != 0 || year%400 == 0)
}

func TimeMonthDaysEx(t time.Time) int {
	cy, cm, _ := t.Date()
	return TimeMonthDays(cy, cm)
}

func TimeMonthDays(year int, month time.Month) int {
	if month >= time.January && month <= time.December {
		if month == time.February && TimeIsLeapYear(year) {
			return 29
		} else {
			return _TimeYearMonthDays[month-1]
		}
	}
	return -1
}

func TimeFormatInLocation(tm time.Time, layout string) string {
	return tm.In(time.Local).Format(layout)
}

func TimeParseInLocation(tm, layout string) (time.Time, error) {
	return time.ParseInLocation(layout, tm, time.Local)
}

// TimeLatelyNWeekDay 获取最近 n 个周的指定周的日期, e 只有在 tl 值为 TimeLatelyBefore 和 TimeLatelyAfter 有效。
func TimeLatelyNWeekDay(t time.Time, w time.Weekday, n uint, tl TimeLately, e bool) time.Time {
	var offset int
	switch tl {
	case TimeLatelyBefore:
		offset = int(w - t.Weekday())
		if offset > 0 || (!e && offset == 0) {
			offset -= 7
		}
		offset -= int(n-1) * 7
	case TimeLatelyAfter:
		offset = int(w - t.Weekday())
		if offset < 0 || (!e && offset == 0) {
			offset += 7
		}
		offset += int(n-1) * 7
	case _TimeLatelyIn:
		if w == time.Sunday {
			w = 7 // = time.Saturday + 1
		}
		cw := t.Weekday()
		if cw == time.Sunday {
			cw = 7 // = time.Saturday + 1
		}
		offset = int(w - cw)
	}
	if offset != 0 {
		t = t.AddDate(0, 0, offset)
	}
	return t
}

// TimeLatelyWeekDay 获取最近一个周的指定周的日期, e 只有在 tl 值为 TimeLatelyBefore 和 TimeLatelyAfter 有效。
func TimeLatelyWeekDay(t time.Time, w time.Weekday, tl TimeLately, e bool) time.Time {
	return TimeLatelyNWeekDay(t, w, 1, tl, e)
}

// TimeThisWeekDay 获取本一个周的指定周的日期
func TimeThisWeekDay(t time.Time, w time.Weekday) time.Time {
	return TimeLatelyNWeekDay(t, w, 1, _TimeLatelyIn, true)
}

// TimeLatelyNMonthDay 获取最近 n 个月的指定日的日期, day 值为 -1 时候表示最后一天
func TimeLatelyNMonthDay(t time.Time, dd int, n uint, tl TimeLately, e bool) time.Time {
	n = n - 1
	cy, cm, cd := t.Date()
	switch tl {
	case TimeLatelyBefore:
		if dd > 0 && (dd > cd || (!e && dd == cd)) {
			n = n + 1
		}
		costYear, costMonth := int(n/12), time.Month(n%12)
		cy -= costYear
		if sub := cm - costMonth; sub <= 0 {
			cm, cy = sub+12, cy-1
		} else if costMonth > time.Month(0) {
			cm -= costMonth
		}
	case TimeLatelyAfter:
		if dd > 0 && (dd < cd || (!e && dd == cd)) {
			n = n + 1
		}
		costYear, costMonth := int(n/12), time.Month(n%12)
		cy += costYear
		if add := costMonth + cm; add > 12 {
			cm, cy = add-12, cy+1
		} else if costMonth > time.Month(0) {
			cm += costMonth
		}
	case _TimeLatelyIn:
	}
	if dd < 1 {
		dd = 1000
	}
	var md = TimeMonthDays(cy, cm)
	if md < dd {
		dd = md
	}
	return time.Date(cy, cm, dd, t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), t.Location())
}

// TimeLatelyMonthDay 获取最近一个月的指定日的日期, day 值为 -1 时候表示最后一天
func TimeLatelyMonthDay(t time.Time, dd int, tl TimeLately, e bool) time.Time {
	return TimeLatelyNMonthDay(t, dd, 1, tl, e)
}

// TimeThisMonthDay 获取本月的指定周的日期
func TimeThisMonthDay(t time.Time, dd int) time.Time {
	return TimeLatelyNMonthDay(t, dd, 1, _TimeLatelyIn, true)
}

func TimeClearZero(v time.Time, mask uint16) time.Time {
	var d time.Duration
	if mask&TimeClearNSecondMask > 0 {
		d += time.Duration(v.Nanosecond())
	}
	if mask&TimeClearSecondMask > 0 {
		d += time.Duration(v.Second()) * time.Second
	}
	if mask&TimeClearMinuteMask > 0 {
		d += time.Duration(v.Minute()) * time.Minute
	}
	if mask&TimeClearHourMask > 0 {
		d += time.Duration(v.Hour()) * time.Hour
	}
	return v.Add(-d)
}
