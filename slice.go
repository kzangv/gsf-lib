package glib

import (
	"golang.org/x/exp/constraints"
	"sort"
)

/* 切片数据转换 *******************/

// Slice2Slice 切片 转 切片
func Slice2Slice[T, R any](arr []T, get func(*T) R) []R {
	ret := make([]R, 0, len(arr))
	for i := range arr {
		ret = append(ret, get(&arr[i]))
	}
	return ret
}

// Slice2SliceFilter 切片 转 切片, 并过滤对应数据
func Slice2SliceFilter[T, R any](arr []T, get func(*T) (R, bool)) []R {
	ret := make([]R, 0, len(arr))
	for i := range arr {
		if v, ok := get(&arr[i]); ok {
			ret = append(ret, v)
		}
	}
	return ret
}

// Slice2Map 切片 转 map
func Slice2Map[E any, K constraints.Ordered](s []E, fn func(v *E) K) map[K]*E {
	r := make(map[K]*E, len(s))
	for k := range s {
		r[fn(&s[k])] = &s[k]
	}
	return r
}

// Slice2MapFilter 切片 转 map, 并过滤对应数据
func Slice2MapFilter[E any, K constraints.Ordered](s []E, fn func(v *E) (K, bool)) map[K]*E {
	r := make(map[K]*E, len(s))
	for sk := range s {
		if k, ok := fn(&s[sk]); ok {
			r[k] = &s[sk]
		}
	}
	return r
}

// Slice2MapEx 切片 转 map
func Slice2MapEx[T, V any, K constraints.Ordered](s []T, get func(*T) (K, V)) map[K]V {
	ret := make(map[K]V, len(s))
	for i := range s {
		k, v := get(&s[i])
		ret[k] = v
	}
	return ret
}

// Slice2MapExFilter 切片 转 map, 并过滤对应数据
func Slice2MapExFilter[T, V any, K constraints.Ordered](s []T, get func(*T) (K, V, bool)) map[K]V {
	ret := make(map[K]V, len(s))
	for i := range s {
		if k, v, ok := get(&s[i]); ok {
			ret[k] = v
		}
	}
	return ret
}

// Slice2Set 切片结构体转换为 map[结构体Field]struct{}
func Slice2Set[E any, K constraints.Ordered](s []E, fn func(v *E) K) Set[K] {
	r := make(Set[K], len(s))
	for k := range s {
		r.Add(fn(&s[k]))
	}
	return r
}

// Slice2SetFilter 切片结构体转换为 map[结构体Field]struct{}
func Slice2SetFilter[E any, K constraints.Ordered](s []E, fn func(v *E) (K, bool)) Set[K] {
	r := make(Set[K], len(s))
	for sk := range s {
		if k, ok := fn(&s[sk]); ok {
			r.Add(k)
		}
	}
	return r
}

/* 切片一般操作 *******************/

// SliceUnique 切片去重
func SliceUnique[E constraints.Ordered](v []E) []E {
	s := make(Set[E], len(v))
	for k := range v {
		s.Add(v[k])
	}
	return s.ToSlice()
}

// SliceForEachIdx 切片遍历
func SliceForEachIdx(l int, handle func(i int) bool) bool {
	for i := 0; i < l; i++ {
		if !handle(i) {
			return false
		}
	}
	return true
}

// SliceForEach 切片遍历
func SliceForEach[T any](arr []T, handle func(T) bool) bool {
	for _, v := range arr {
		if !handle(v) {
			return false
		}
	}
	return true
}

// SliceSortAsc 切片排序（正序）
func SliceSortAsc[T constraints.Ordered](v []T) []T {
	sort.Slice(v, func(i, j int) bool {
		return v[i] < v[j]
	})
	return v
}

// SliceSortDesc 切片排序（倒序）
func SliceSortDesc[T constraints.Ordered](v []T) []T {
	sort.Slice(v, func(i, j int) bool {
		return v[i] > v[j]
	})
	return v
}

/* 切片集合操作 *******************/

type _SliceMark[E constraints.Ordered] map[E]uint32

func (m *_SliceMark[E]) mark(sliceList [][]E) {
	sl := len(sliceList)
	if sl > 32 {
		panic("the sliceList size too big")
	}
	if sl == 0 {
		return
	}
	*m = make(_SliceMark[E])
	for i := range sliceList {
		for j := range sliceList[i] {
			v := sliceList[i][j]
			if mv, ok := (*m)[v]; !ok {
				(*m)[v] = uint32(1 << i)
			} else if p := uint32(1 << i); p&mv == 0 {
				(*m)[v] = mv | p
			}
		}
	}
}

func (m *_SliceMark[E]) filter(f func(uint32) bool) []E {
	ret := make([]E, 0, len(*m))
	for k := range *m {
		if f((*m)[k]) {
			ret = append(ret, k)
		}
	}
	return ret
}

// SliceIntersect 切片交集
func SliceIntersect[E constraints.Ordered](sliceList ...[]E) []E {
	mask := uint32(1<<len(sliceList)) - 1
	return SliceFilter(func(vMask uint32) bool {
		return vMask == mask
	}, sliceList)
}

// SliceDifference 切片差集
func SliceDifference[E constraints.Ordered](sliceList ...[]E) []E {
	return SliceFilter(func(vMask uint32) bool {
		return BitCount(vMask) == 1
	}, sliceList)
}

// SliceUnion 切片减法
func SliceUnion[E constraints.Ordered](sliceList ...[]E) []E {
	return SliceFilter(func(vMask uint32) bool {
		return true
	}, sliceList)
}

// SliceSub 切片减法
func SliceSub[E constraints.Ordered](sliceList ...[]E) []E {
	return SliceFilter(func(vMask uint32) bool {
		return vMask == 1
	}, sliceList)
}

// SliceFilter 切片扩展方法
func SliceFilter[E constraints.Ordered](handel func(uint32) bool, sliceList [][]E) []E {
	var tmp _SliceMark[E]
	tmp.mark(sliceList)
	return tmp.filter(handel)
}

// SliceAdd 切片并集
func SliceAdd[E constraints.Ordered](sliceList ...[]E) []E {
	return SliceUnion(sliceList...)
}

type _SliceMarkItem struct {
	pos  [2]uint8
	mask uint32
}
type _SliceMarkEx[K constraints.Ordered, E any] map[K]*_SliceMarkItem

func (m *_SliceMarkEx[K, E]) mark(key func(*E) K, sliceList ...[]E) {
	sl := len(sliceList)
	if sl > 32 {
		panic("the sliceList size too big")
	}
	if sl == 0 {
		return
	}
	l := 0
	for i := range sliceList {
		l += len(sliceList[i])
	}
	*m = make(_SliceMarkEx[K, E], l)
	for i := range sliceList {
		for j := range sliceList[i] {
			v := key(&sliceList[i][j])
			if mv, ok := (*m)[v]; !ok {
				(*m)[v] = &_SliceMarkItem{pos: [2]uint8{uint8(i), uint8(j)}, mask: uint32(1 << i)}
			} else if p := uint32(1 << i); p&mv.mask == 0 {
				(*m)[v].mask = mv.mask | p
			}
		}
	}
}

func (m *_SliceMarkEx[K, E]) filter(f func(uint32) bool) [][]uint8 {
	ret := make([][]uint8, 0, len(*m))
	for k := range *m {
		if f((*m)[k].mask) {
			ret = append(ret, (*m)[k].pos[:])
		}
	}
	return ret
}

// SliceIntersectEx 切片交集
func SliceIntersectEx[K constraints.Ordered, E any](key func(*E) K, sliceList ...[]E) []*E {
	var tmp _SliceMarkEx[K, E]
	tmp.mark(key, sliceList...)
	mask := uint32(1<<len(sliceList)) - 1
	pos := tmp.filter(func(vMask uint32) bool {
		return vMask == mask
	})

	ret := make([]*E, 0, len(pos))
	for _, v := range pos {
		ret = append(ret, &sliceList[v[0]][v[1]])
	}
	return ret
}

// SliceDifferenceEx 切片差集
func SliceDifferenceEx[K constraints.Ordered, E any](key func(*E) K, sliceList ...[]E) []*E {
	var tmp _SliceMarkEx[K, E]
	tmp.mark(key, sliceList...)
	pos := tmp.filter(func(vMask uint32) bool {
		return BitCount(vMask) == 1
	})
	ret := make([]*E, 0, len(pos))
	for _, v := range pos {
		ret = append(ret, &sliceList[v[0]][v[1]])
	}
	return ret
}

// SliceUnionEx 切片并集
func SliceUnionEx[E any, K constraints.Ordered](key func(*E) K, sliceList ...[]E) []*E {
	var ret []*E
	sl := len(sliceList)
	if sl > 0 {
		l := 0
		for i := range sliceList {
			l += len(sliceList[i])
		}
		// 标记数据
		tmp := make(map[K]*E, l)
		for i := range sliceList {
			for j := range sliceList[i] {
				tmp[key(&sliceList[i][j])] = &sliceList[i][j]
			}
		}
		// 填充交集数据
		ret = make([]*E, 0, len(tmp))
		for k := range tmp {
			ret = append(ret, tmp[k])
		}
	}
	return ret
}
