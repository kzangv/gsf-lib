package glib

import (
	"encoding/json"
	"testing"
)

func TestType(t *testing.T) {

	t.Logf("RawJSON:----------")
	v, _ := json.Marshal(JsonRawString("{\"k\":1}"))
	t.Logf("JsonRawString: " + string(v))
	v, _ = json.Marshal(JsonRawMarshalString("{\"k\":1}"))
	t.Logf("JsonRawMarshalString: " + string(v))
	v, _ = json.Marshal("{\"k\":1}")
	t.Logf("string: " + string(v))

	t.Logf("RawByte:----------")
	v, _ = json.Marshal(JsonRawByte("{\"k\":1}"))
	t.Logf("JsonRawByte: " + string(v))
	v, _ = json.Marshal(JsonRawMarshalByte("{\"k\":1}"))
	t.Logf("JsonRawMarshalByte: " + string(v))
	v, _ = json.Marshal([]byte("{\"k\":1}"))
	t.Logf("[]byte: " + string(v))
}
