package glib

import (
	"golang.org/x/exp/constraints"
)

/* map 相关特殊结构 *******************/

type Map4Any map[string]interface{}

type Set[T constraints.Ordered] map[T]EmptyStruct

func (m Set[T]) Add(v T) {
	m[v] = EmptyStruct{}
}

func (m Set[T]) Has(v T) bool {
	_, ok := m[v]
	return ok
}

func (m Set[T]) ToSlice() []T {
	return MapKeys(m)
}

/* map 数据转换 *******************/

// Map2Slice map 转 切片
func Map2Slice[T, R any, K constraints.Ordered](s map[K]T, fn func(k K, v T) R) []R {
	r := make([]R, 0, len(s))
	for sk := range s {
		r = append(r, fn(sk, s[sk]))
	}
	return r
}

// Map2SliceFilter map 转 切片, 并过滤对应数据
func Map2SliceFilter[T, R any, K constraints.Ordered](val map[K]T, get func(T) (R, bool)) []R {
	ret := make([]R, 0, len(val))
	for i := range val {
		if v, ok := get(val[i]); ok {
			ret = append(ret, v)
		}
	}
	return ret
}

/* map 一般操作 *******************/

// MapKeys 获取 keys
func MapKeys[K constraints.Ordered, V any](m map[K]V) []K {
	r := make([]K, 0, len(m))
	for k := range m {
		r = append(r, k)
	}
	return r
}

// MapValues 获取 values
func MapValues[K constraints.Ordered, V any](m map[K]V) []V {
	r := make([]V, 0, len(m))
	for k := range m {
		r = append(r, m[k])
	}
	return r
}

// MapMerge 多个 map merge
func MapMerge[K constraints.Ordered, V any](maps ...map[K]V) map[K]V {
	l := 0
	for _, m := range maps {
		l += len(m)
	}
	r := make(map[K]V, l)
	for _, m := range maps {
		for k, v := range m {
			if _, ok := r[k]; !ok {
				r[k] = v
			}
		}
	}
	return r
}

func MapForEach[K constraints.Ordered, T any](val map[K]T, handle func(K) error) error {
	for k := range val {
		if err := handle(k); err != nil {
			return err
		}
	}
	return nil
}
