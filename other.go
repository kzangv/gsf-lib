package glib

import (
	"crypto/md5"
	"encoding/hex"
	"golang.org/x/exp/constraints"
)

func BitCount[V constraints.Unsigned](v V) int {
	c := 0
	for ; v != 0; c++ {
		v &= v - 1
	}
	return c
}

// Md5 生成32位MD5摘要
func Md5(str string) string {
	m := md5.New()
	m.Write([]byte(str))
	return hex.EncodeToString(m.Sum(nil))
}
