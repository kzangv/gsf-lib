package glib

import (
	"fmt"
	"golang.org/x/exp/constraints"
	"reflect"
	"strconv"
)

func IntegerToString[R constraints.Integer](v R) string {
	return fmt.Sprintf("%d", v)
}

func FloatToString[R constraints.Float](v R, prec int) string {
	return fmt.Sprintf("%."+fmt.Sprintf("%d", prec)+"f", v)
}

func StringToInteger(v string) int {
	ret, _ := strconv.Atoi(v)
	return ret
}

func StringToFloat(v string) float64 {
	val, _ := strconv.ParseFloat(v, 64)
	return val
}

func ToString(val interface{}, limit ...int) string {
	rv := reflect.ValueOf(val)
	switch rv.Kind() {
	case reflect.String:
		return rv.String()
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return fmt.Sprintf("%d", val)
	case reflect.Float32, reflect.Float64:
		if len(limit) > 0 {
			return fmt.Sprintf("%."+fmt.Sprintf("%d", limit[0])+"f", val)
		}
		return fmt.Sprintf("%f", val)
	case reflect.Invalid:
		return ""
	default:
		panic(fmt.Sprintf("[public.ToString][code:1] type:%T data:%v", val, val))

	}
}

func ToInt(val interface{}) int {
	rv := reflect.ValueOf(val)
	switch rv.Kind() {
	case reflect.String:
		v, _ := strconv.Atoi(rv.String())
		return v
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int, reflect.Int64:
		return int(rv.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return int(rv.Uint())
	case reflect.Float32, reflect.Float64:
		return int(rv.Float())
	case reflect.Invalid:
		return 0
	default:
		panic(fmt.Sprintf("[public.ToInt][code:1] type:%T data:%v", val, val))
	}
}

func ToFloat(val interface{}) float64 {
	rv := reflect.ValueOf(val)
	switch rv.Kind() {
	case reflect.String:
		v, _ := strconv.ParseFloat(rv.String(), 64)
		return v
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int, reflect.Int64:
		return float64(rv.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return float64(rv.Uint())
	case reflect.Float32, reflect.Float64:
		return rv.Float()
	case reflect.Invalid:
		return 0
	default:
		panic(fmt.Sprintf("[public.ToFloat][code:1] type:%T data:%v", val, val))
	}
}
